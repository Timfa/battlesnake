var playButton = 
{
    bots: false,

    botsOnly: function()
    {
        this.transform.scale.x = 0.5;
        this.transform.scale.y = 0.5;
        this.bots = true;
    },

    onClick: function()
    {
        enterGameScene(this.bots);

        GameAudio.play("tap");
    }
}