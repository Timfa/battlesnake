/***
 * All images used in the game are  initialized here
 */

var s_snakeHead = new image("images/snakeHead.png", 4, 4);
var s_snakeBody = new image("images/snakeBody.png", 4, 4);
var s_base = new image("images/bases.png", 4, 4);
var s_gem = new image("images/gems.png", 72, 6);
var s_playButton = new image("images/playbutton.png");

var s_playerCountText = new image("images/players.png");
var s_botsOnlyText = new image("images/botsonly.png");
var s_playerCountNumbers = new image("images/playercount.png", 3, 3);