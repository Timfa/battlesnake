/**
 * Player snake
 */

var snakeBody = 
{
    start: function()
    {
        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        this.snakeTeam = -1;
    },

    onDestroy: function()
    {
        if (enemySnake.edibles.contains(this))
        {
            enemySnake.edibles.remove(this);
        }
    },

    onCollision: function(other)
    {
        var doIgnoreSelf = this.ignoreSelf && this.snakeTeam == other.snakeColor;

        if (other.name == "snake" && !doIgnoreSelf && !this.destroyed) 
        {
            other.grow(1, this.renderer.subImage);
            this.destroy();
        }
    },

    collect: function(targetBase, delay)
    {
        this.ignoreCollisions = true;

        timingUtil.delay(delay, function()
        {
            GameAudio.play("pop");

            var scaleUpTween = new TWEEN.Tween(this.transform.scale);

            scaleUpTween.to({x: [1.5, 0.4], y: [1.5, 0.4]}, 200);

            scaleUpTween.easing(TWEEN.Easing.Quadratic.Out);

            scaleUpTween.onComplete(function()
            {
                var particle = GameObject.create(collectParticle, this.transform.position.x, this.transform.position.y, s_gem, 550);

                particle.collect(targetBase);

                this.destroy();
            }, this);
            
            scaleUpTween.start();
        }, this);
    }
}