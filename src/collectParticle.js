var collectParticle = 
{
    start: function()
    {
        this.renderer.subImage = Math.floor(Math.random() * this.renderer.image.subImages);

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        OnGameEnd.addOnce(function()
        {
            this.destroy();
        }, this);
    },

    collect: function(targetBase)
    {
        var movementTween = new TWEEN.Tween(this.transform.position);

        var midPoint = this.transform.position.lerpTo(targetBase.transform.position, 0.5);

        var dist = this.transform.position.distanceTo(targetBase.transform.position);

        var randomAddition = new vector((-dist / 2) + (Math.random() * dist), (-dist / 2) + (Math.random() * dist))

        midPoint = midPoint.add(randomAddition);

        movementTween.to({x: [midPoint.x, targetBase.transform.position.x], y: [midPoint.y, targetBase.transform.position.y]}, 750 + (500 * Math.random()));

        movementTween.easing(TWEEN.Easing.Quadratic.InOut);

        movementTween.interpolation(TWEEN.Interpolation.Bezier);

        movementTween.onComplete(function()
        {
            targetBase.scorePoint();
            
            this.destroy();

            GameAudio.play("glass");
        }, this);

        movementTween.start();
    }
}