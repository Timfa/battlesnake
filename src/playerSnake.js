/**
 * Player snake
 */

var playerSnake = 
{

	update: function(deltaTime)
	{
        this.base.update.call(this, deltaTime);

        if (GameInput.mouseHeld(GameInput.MouseButtons.Left))
        {
            this.steerTo(GameInput.mousePosition);
        }
	}
}