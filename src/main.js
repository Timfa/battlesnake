/**
 * Main script
 */

window.onload = init;

var player = null;
var enemy = null;
var green = null;
var blue = null;

var playerBase = null;
var enemyBase = null;

var targetScore = 100;

var OnGameEnd = new signal();

var p1 = null;
var p2 = null;

var playerCount = 2;

var twoPlayersBtn, threePlayersBtn, fourPlayersBtn;
var btext, ptext, num1, num2, num3;
function DoInheritance()
{
    playerSnake = GameObject.inherit(snake, playerSnake);
    enemySnake = GameObject.inherit(snake, enemySnake);
}

function init()
{
    DoInheritance();
    //Game.initialize(xpos, ypos, width, height, fps, callback)
    // -2 to account for borders
    var width = window.innerWidth - 2;
    var height = window.innerHeight - 2;

    Game.initialize(0, 0, width, height, 600, gameStart);
}

function gameStart()
{
	GameWindow.backgroundColor = {r:100, g:149, b:237};

	Game.setUpdateFunction(update); // Let the game engine know we want to link our 'update()' to the game's update step.

	GameAudio.init(sounds, soundsFolder);

    p1 = GameObject.create(playButton, GameWindow.width / 2, GameWindow.height / 2, s_playButton, 500);
    p2 = GameObject.create(playButton, (GameWindow.width / 2) + 62, (GameWindow.height / 2) + 154, s_playButton, 500);
    p2.botsOnly();

    twoPlayersBtn = GameObject.create({onClick: function(){SetPlayerCount(2);}}, GameWindow.width / 2 - 64, GameWindow.height / 3, s_snakeBody, 500);
    threePlayersBtn = GameObject.create({onClick: function(){SetPlayerCount(3);}}, GameWindow.width / 2, GameWindow.height / 3, s_snakeBody, 500);
    fourPlayersBtn = GameObject.create({onClick: function(){SetPlayerCount(4);}}, GameWindow.width / 2 + 64, GameWindow.height / 3, s_snakeBody, 500);

    threePlayersBtn.renderer.subImage = 1;
    fourPlayersBtn.renderer.subImage = 1;

    btext = GameObject.create({}, GameWindow.width / 2 - 32, GameWindow.height / 2 + 154, s_botsOnlyText, 600);
    ptext = GameObject.create({}, GameWindow.width / 2, GameWindow.height/3 - 34, s_playerCountText, 600);
    num1 = GameObject.create({}, GameWindow.width / 2 - 64, GameWindow.height / 3, s_playerCountNumbers, 600);
    num2 = GameObject.create({}, GameWindow.width / 2, GameWindow.height / 3, s_playerCountNumbers, 600);
    num3 = GameObject.create({}, GameWindow.width / 2 + 64, GameWindow.height / 3, s_playerCountNumbers, 600);

    num1.renderer.subImage = 0;
    num2.renderer.subImage = 1;
    num3.renderer.subImage = 2;

    parent.destroy();

    SetPlayerCount(2);
}

function SetPlayerCount(num)
{
    playerCount = num;

    twoPlayersBtn.renderer.subImage = 1;
    threePlayersBtn.renderer.subImage = 1;
    fourPlayersBtn.renderer.subImage = 1;

    if(num == 2)
        twoPlayersBtn.renderer.subImage = 0;
    
    if(num == 3)
        threePlayersBtn.renderer.subImage = 0;
    
    if(num == 4)
        fourPlayersBtn.renderer.subImage = 0;
}

function enterGameScene(botsOnly)
{
    p1.destroy();
    p2.destroy();

    num1.destroy();
    num2.destroy();
    num3.destroy();
    ptext.destroy();
    btext.destroy();

    twoPlayersBtn.destroy();
    threePlayersBtn.destroy();
    fourPlayersBtn.destroy();

    console.log("Starting game" + (botsOnly? " with only bots" : ""))

    GameObject.create(gemSpawner, 500, 500);

    player = GameObject.create(botsOnly? enemySnake : playerSnake, GameWindow.width - 64, 64, s_snakeHead, 500);
    playerBase = GameObject.create(snakeBase, GameWindow.width - 64, 64, s_base, 600);
    player.transform.rotation = 180;
    playerBase.setTeam(0);
    player.team = 0;
    player.snakeBase = playerBase;

    enemy = GameObject.create(enemySnake, 64, GameWindow.height - 64, s_snakeHead, 500);
    enemyBase = GameObject.create(snakeBase, 64, GameWindow.height - 64, s_base, 600);
    enemyBase.setTeam(1);
    enemy.team = 1;
    enemy.snakeBase = enemyBase;

    if(playerCount > 2)
    {
        green = GameObject.create(enemySnake, 64, 64, s_snakeHead, 500);
        greenBase = GameObject.create(snakeBase, 64, 64, s_base, 600);
        green.transform.rotation = 180;
        greenBase.setTeam(2);
        green.team = 2;
        green.snakeBase = greenBase;
    }

    if(playerCount > 3)
    {
        yellow = GameObject.create(enemySnake, GameWindow.width - 64, GameWindow.height - 64, s_snakeHead, 500);
        yellowBase = GameObject.create(snakeBase, GameWindow.width - 64, GameWindow.height - 64, s_base, 600);
        yellowBase.setTeam(3);
        yellow.team = 3;
        yellow.snakeBase = yellowBase;
    }
}

function cleanGameScene()
{
    OnGameEnd.dispatch();

    player.destroy();
    enemy.destroy();
    playerBase.destroy();
    enemyBase.destroy();

    player = null;
    enemy = null;
    playerBase = null;
    enemyBase = null;
}

function update(deltaTime)
{  
   
}