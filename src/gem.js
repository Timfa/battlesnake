/**
 * Snake Food
 */

var gem =
{
    start: function()
    {
        this.renderer.subImage = Math.floor(Math.random() * this.renderer.image.subImages);

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        this.shakeLength = 1000;
        this.shakes = 4;
        this.shakeAngle = 45;

        this.minShakeTimer = 5;
        this.maxShakeTimer = 20;
        this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));
        this.currentShakeTimer = 0;

        enemySnake.edibles.push(this);

        GameAudio.play("bubble");
    },

    update: function(deltaTime)
    {
        this.currentShakeTimer += deltaTime;

        if(this.currentShakeTimer >= this.shakeTimer)
        {
            this.shake();

            this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));

            this.currentShakeTimer = 0;
        }
    },

    onDestroy: function()
    {
        gemSpawner.currentActive--;

        enemySnake.edibles.remove(this);
    },

    onCollision: function(other)
    {
        if (other.name == "snake" && !this.destroyed) 
        {
            other.grow(1, -1);
            this.destroy();
        }
    },

    shake: function(callback, context)
    {
        var singleShakeTime = this.shakes / this.shakeLength;

        var shakeTween = new TWEEN.Tween(this.transform);

        var angles = [];

        var offset = Math.round(Math.random() * 5);

        for (var i = 0; i < this.shakes; i++)
        {
            var intensiy = 1 - (i / this.shakes);

            // Direction of the shake (is 'i' odd?)
            var dir = !((i + offset) % 2)? -1 : 1;

            var angleRad = this.shakeAngle;

            var angle = angleRad * dir * intensiy;

            angles.push(angle);
        }

        angles.push(0);

        shakeTween.to({rotation: angles}, this.shakeLength);

        shakeTween.easing(TWEEN.Easing.Quadratic.InOut);

        if (callback)
        {
            shakeTween.onComplete.addOnce(callback, context);
        }

        shakeTween.start();
    }
}