/**
 * Enemy snake
 */

var enemySnake = 
{
    edibles: [],
 
    start: function()
    {
        this.base.start.call(this);

        this.snakeColor = 1;

        this.target = null;

        this.minRange = 50;

        this.turnBackPercentage = 0.4;
        this.minTurnBackAmount = 5;
        this.maxTurnBackAmount = 15;

        this.snakeBase = null;

        this.state = this.collectGems;
    },

	update: function(deltaTime)
	{
        this.base.update.call(this, deltaTime);

        this.snakeColor = this.team;

        this.state();
	},

    returnGems: function()
    {
        this.steerTo(this.snakeBase.transform.position);

        var targetAmount = Math.clamp(this.snakeBase.points * this.turnBackPercentage, this.minTurnBackAmount, this.maxTurnBackAmount);

        if(this.tailPieces.length < targetAmount * 0.5)
            this.state = this.collectGems;
    },

    collectGems: function()
    {
        if(this.target == null || this.target.transform.position.distanceTo(this.transform.position) > this.minRange)
            this.target = this.findNearestEdible();

        if (this.target && this.target.destroyed)
        {
            this.target = null;
        }

        if (this.target != null)
        {
            this.steerTo(this.target.transform.position);
        }

        var targetAmount = Math.clamp(this.snakeBase.points * this.turnBackPercentage, this.minTurnBackAmount, this.maxTurnBackAmount);

        if (this.tailPieces.length > targetAmount)
        {
            this.state = this.returnGems;

            this.target = null;
        }
    },

    collectTail: function(targetBase)
	{
        this.base.collectTail.call(this, targetBase);

        if (this.state == this.returnGems)
        {
            this.state = this.collectGems;
        }
    },

    findNearestEdible: function()
    {
        var currentDist = 10000;
        var newTarget = null;

        enemySnake.edibles = enemySnake.edibles.filter(function(item)
        {
            return !item.destroyed;
        })

        for (var i = 0; i < enemySnake.edibles.length; i++)
        {
            var edible = enemySnake.edibles[i];

            var distTo = edible.transform.position.distanceTo(this.transform.position);

            for(var s = 0; s < snake.allSnakes.length; s++)
            {
                if(snake.allSnakes[s] != this && snake.allSnakes[s].target == edible)
                    distTo = 1000;
                
                var oD = snake.allSnakes[s].transform.position.distanceTo(edible.transform.position);

                if(snake.allSnakes[s] != this && oD < 250 && oD < distTo)
                    distTo = 10000;        
            }

            if (distTo < currentDist && distTo >= this.minRange)
            {    
                currentDist = distTo;

                newTarget = edible;
            }
        }

        for(var i = 0; i < snake.allSnakes.length; i++)
        {
            var targetAmount = Math.clamp(this.snakeBase.points * this.turnBackPercentage, this.minTurnBackAmount, this.maxTurnBackAmount);

            if(snake.allSnakes[i] != this && snake.allSnakes[i] && snake.allSnakes[i].tailPieces.length > this.tailPieces.length * 0.5 && snake.allSnakes[i].tailPieces.length > targetAmount * 0.7)
            {
                var sD = snake.allSnakes[i].snakeBase.transform.position.distanceTo(this.transform.position);
                var oD = snake.allSnakes[i].snakeBase.transform.position.distanceTo(snake.allSnakes[i].transform.position);

                if(sD < oD || oD > 300)
                {
                    for(var t = 0; t < snake.allSnakes[i].tailPieces.length; t++)
                    {
                        var edible = snake.allSnakes[i].tailPieces [t];
                        
                        var distTo = edible.transform.position.distanceTo(this.transform.position);
            
                        if (distTo < currentDist && distTo >= this.minRange)
                        {
                            currentDist = distTo;
            
                            newTarget = edible;
                        }  
                    }
                }
            }
        }

        currentDist = 10000;

        return newTarget;
    }
}