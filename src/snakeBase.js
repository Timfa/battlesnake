var snakeBase = 
{
    start: function()
    {
        this.points = 0;
    },

    setTeam: function(teamNumber)
    {
        this.renderer.subImage = teamNumber;
    },

    onCollision: function(other)
    {
        if (other.name == "snake" && other.renderer.subImage == this.renderer.subImage)
        {
            other.collectTail(this);
        }
    },

    scorePoint: function()
    {
        this.points++;
    },

    draw: function()
    {
        this.defaultDraw();

        canvas.drawText(this.points, this.transform.position.x, this.transform.position.y, 20, "Arial", "black");
    }
}