/**
Base Snake 
*/

var snake = 
{
	allSnakes: [],

	start: function()
	{
		snake.allSnakes.push(this);

        this.speed = 100;

		this.steerAngle = 0;

		this.steerSpeed = 150;

		this.tailPieces = [];

		this.tailPieceDistance = 20;

		this.positionHistory = [];

		this.name = "snake";

		this.snakeColor = 0;

		this.team = 0;
	},

	onDestroy: function()
	{
		for (var i = 0; i < this.tailPieces.length; i++)
        {
            this.tailPieces[i].destroy();
        }
	},

	grow: function(amount, color)
	{
		if(color == -1)
			color = this.team;

		GameAudio.play("bite");

		if (this.tailPieces.length > 0)
		{
			var tailPiece = this.tailPieces[this.tailPieces.length - 1];

			var pos = tailPiece.transform.position.add(tailPiece.transform.back().stretch(10000));
		}
		else
		{
			var pos = this.transform.position.add(this.transform.back().stretch(10000));
		}

		var piece = GameObject.create(snakeBody, pos.x, pos.y, s_snakeBody, 10);

		if (this.tailPieces.length < 4)
		{
			piece.ignoreSelf = true;
		}

		piece.ignoreCollisions = true;

		piece.renderer.subImage = color;

		piece.snakeTeam = this.team;

		piece.spawned = false;

		this.tailPieces.push(piece);

		for (var i = 0; i < this.tailPieces.length; i++)
		{
			this.tailPieces[i].renderer.layerDepth = 499 - i;
		}

		var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.3, 1], y: [1.3, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

		var tailPiecesCopy = this.tailPieces.slice();

		for (var i = 0; i < tailPiecesCopy.length; i++)
		{
			timingUtil.delay(i * 50, function()
			{
				var scaleUpTween = new TWEEN.Tween(this.transform.scale);

				scaleUpTween.to({x: [1.3, 1], y: [1.3, 1]}, 500);

				scaleUpTween.easing(TWEEN.Easing.Back.Out);

				scaleUpTween.start();

				this.ignoreCollisions = false;

				this.spawned = true;

			}, tailPiecesCopy[i]);
		}
	},

	steerTo: function(point)
	{
		this.steerAngle = this.transform.forward().deltaAngle(point.subtract(this.transform.position));
	},

	update: function(deltaTime)
	{
		this.renderer.subImage = this.snakeColor;

		if (this.transform.position.x < 0 || this.transform.position.x > GameWindow.width || this.transform.position.y < 0 || this.transform.position.y > GameWindow.height)
		{
			this.steerTo(new vector(GameWindow.width / 2, GameWindow.height / 2));
		}

		this.transform.position = this.transform.position.add(this.transform.forward().stretch(this.speed * deltaTime));

		var currentSteer = this.steerSpeed * deltaTime * Math.sign(this.steerAngle);

		this.steerAngle -= currentSteer;

		this.transform.rotation -= currentSteer;

		this.setTailPiecesPositions();
	},

	collectTail: function(targetBase)
	{
		for (var i = 0; i < this.tailPieces.length; i++)
		{
			if (this.tailPieces[i] && !this.tailPieces[i].destroyed)
			{
				this.tailPieces[i].collect(targetBase, i * 50);
			}
		}

		this.tailPieces = [];
	},

	setTailPiecesPositions: function()
	{
		for (var i = 0; i < this.tailPieces.length; i++)
		{
			if (this.tailPieces[i] && !this.tailPieces[i].destroyed)
			{
				var dist = 0;

				if (i == 0)
				{
					var target = this.transform.position.add(this.transform.back().stretch(this.tailPieceDistance * 0.5));

					this.tailPieces[i].transform.pointTowards(this.transform.position);

					dist = this.tailPieces[i].transform.position.distanceTo(this.transform.position) - this.tailPieceDistance;
				}
				else
				{
					var target = this.tailPieces[i - 1].transform.position.add(this.tailPieces[i - 1].transform.back().stretch(this.tailPieceDistance * 0.5));

					this.tailPieces[i].transform.pointTowards(target);

					dist = this.tailPieces[i].transform.position.distanceTo(this.tailPieces[i - 1].transform.position) - this.tailPieceDistance;
				}

				this.tailPieces[i].transform.position = this.tailPieces[i].transform.position.add(this.tailPieces[i].transform.forward().stretch(dist));
			}
			else
			{
				var removed = this.tailPieces.splice(i, this.tailPieces.length - (i)); 

				for (var j = 0; j < removed.length; j++)
				{
					if (!removed[j].spawned || removed[j].transform.position.x < 0 || removed[j].transform.position.y < 0 || removed[j].transform.position.x > GameWindow.width || removed[j].transform.position.y > GameWindow.height)
					{
						removed[j].destroy();
					}
					else
					{
						enemySnake.edibles.push(removed[j]);
						removed[j].ignoreSelf = false;
					}
				}

				return;
			}
		}
	}
}